import 'package:flutter/material.dart';

import 'package:home_user_wth_drawer/Pages/home/fragments/firstFragment.dart';
import 'package:home_user_wth_drawer/Pages/home/fragments/secondFragment.dart';


class DrawerItem {

  String title;
  IconData icon;


  DrawerItem(this.title, this.icon);
}


class HomeEmployeePage extends StatefulWidget {
  final drawerItems=[
    new DrawerItem('Visitas Pendientes', Icons.access_time),
    new DrawerItem('Invitación', Icons.event),
    new DrawerItem('Historial de Accesos', Icons.class_)
  ];


  @override
  State<StatefulWidget> createState() {
    return new HomePageState();
  }
}

class HomePageState extends State<HomeEmployeePage> {
  int _selectedDrawerIndex = 0;


  _getDrawerItemWidget(int pos) {
    switch(pos) {
      case 0:
        return new FirstFragment();
      case 1:
        return new SecondFragment();
      default:
        return new Text("Error");
    }
  }

  _onSelectItem(int index) {
    setState(() {
      _selectedDrawerIndex = index;
    });
    Navigator.of(context).pop();
  }


  @override
  Widget build(BuildContext context) {
    var drawerOptions = <Widget>[];

    for(var i = 0; i <widget.drawerItems.length; i++) {
      var drawerItem = widget.drawerItems[i];

      drawerOptions.add(
        new ListTile(
          leading: Icon(drawerItem.icon),
          title: Text(drawerItem.title),
          selected: i == _selectedDrawerIndex, //Condicion que permite saber si el elemento ha sido seleccionado
          onTap: () =>_onSelectItem(i),
        )
      );
    }

    return new Scaffold(
      appBar: AppBar(
        title: Text(widget.drawerItems[_selectedDrawerIndex].title),
      ),
      drawer: new Drawer(
        child: new Column(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text('Aureilio Morales'),
              accountEmail: Text('aurelio.hdz.aguilar@gmail.com'),
              currentAccountPicture: CircleAvatar(
                backgroundColor: Colors.blue,
                child: Text('A',
                style: TextStyle(fontSize: 40.0),),
              ),
            ),
            new Column(children: drawerOptions,)
          ],
        ),
      ),
      body:_getDrawerItemWidget(_selectedDrawerIndex)
    );
  }
}





